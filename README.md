# README #

### What is this repository for? ###

* The Unity Audio recording does not seem to work on the Android platform. So this is an attempt to use the Android sdk directly. 
* Version 0.01
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Credits to

* The Unity plugin structure is based on the [instructions](https://blog.nraboy.com/2014/06/creating-an-android-java-plugin-for-unity3d/) by [Nick Raboy](http://www.nraboy.com/) 
* The Android Recording code is based on [GAST](https://github.com/gast-lib/gast-lib)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

* Tweet @meat_and_fruit if you have any issues or questions

### To Do
* Add a a background task to the plugin. Right now it stall the Unity app until the recording is finished (5 sec)