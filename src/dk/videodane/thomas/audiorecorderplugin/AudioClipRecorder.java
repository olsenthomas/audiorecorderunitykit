package dk.videodane.thomas.audiorecorderplugin;

import java.util.*;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioRecord.OnRecordPositionUpdateListener;
import android.media.MediaRecorder.AudioSource;
import android.os.AsyncTask;
import android.util.Log;


/**
 * record an audio clip and pass it to the listener
 * 
 * @author gmilette
 * 
 */
public class AudioClipRecorder
{
    private static AudioClipRecorder instance;

    private static final String TAG = "AudioClipRecorder";

    private AudioRecord recorder;

    /**
     * state variable to control starting and stopping recording
     */
    private boolean continueRecording;

    public static final int RECORDER_SAMPLERATE_CD = 44100;
    public static final int RECORDER_SAMPLERATE_8000 = 8000;

    private static final int DEFAULT_BUFFER_INCREASE_FACTOR = 3;

    private AsyncTask task;

    private boolean heard;
	
	private List<Short> sound = new ArrayList<Short>();

    public AudioClipRecorder()
    {
        heard = false;
        task = null;
    }

    public AudioClipRecorder(AsyncTask task)
    {
        this.task = task;
    }
    
    public static AudioClipRecorder instance() {
        if(instance == null) {
            instance = new AudioClipRecorder();
        }
        return instance;
    }
    
    public String Test(){
      return "klaphat bob";
    }

    /**
     * records with some default parameters
     */
    public boolean startRecording()
    {
        sound.clear();
        Log.d(TAG, "StartRecording 5 sec");
        return startRecordingForTime(5000,RECORDER_SAMPLERATE_8000,
                AudioFormat.ENCODING_PCM_16BIT);
    }

    /**
     * start recording: set the parameters that correspond to a buffer that
     * contains millisecondsPerAudioClip milliseconds of samples
     */
    public boolean startRecordingForTime(int millisecondsPerAudioClip,
            int sampleRate, int encoding)
    {
        float percentOfASecond = (float) millisecondsPerAudioClip / 1000.0f;
        int numSamplesRequired = (int) ((float) sampleRate * percentOfASecond);
        int bufferSize =
                determineCalculatedBufferSize(sampleRate, encoding,
                        numSamplesRequired);

        return doRecording(sampleRate, encoding, bufferSize,
                numSamplesRequired, DEFAULT_BUFFER_INCREASE_FACTOR);
    }

    /**
     * start recording: Use a minimum audio buffer and a read buffer of the same
     * size.
     */
    public boolean startRecording(final int sampleRate, int encoding)
    {
        int bufferSize = determineMinimumBufferSize(sampleRate, encoding);
        return doRecording(sampleRate, encoding, bufferSize, bufferSize,
                DEFAULT_BUFFER_INCREASE_FACTOR);
    }

    private int determineMinimumBufferSize(final int sampleRate, int encoding)
    {
        int minBufferSize =
                AudioRecord.getMinBufferSize(sampleRate,
                        AudioFormat.CHANNEL_IN_MONO, encoding);
        return minBufferSize;
    }

    /**
     * Calculate audio buffer size such that it holds numSamplesInBuffer and is
     * bigger than the minimum size<br>
     * 
     * @param numSamplesInBuffer
     *            Make the audio buffer size big enough to hold this many
     *            samples
     */
    private int determineCalculatedBufferSize(final int sampleRate,
            int encoding, int numSamplesInBuffer)
    {
        int minBufferSize = determineMinimumBufferSize(sampleRate, encoding);

        int bufferSize;
        // each sample takes two bytes, need a bigger buffer
        if (encoding == AudioFormat.ENCODING_PCM_16BIT)
        {
            bufferSize = numSamplesInBuffer * 2;
        }
        else
        {
            bufferSize = numSamplesInBuffer;
        }

        if (bufferSize < minBufferSize)
        {
            Log.w(TAG, "Increasing buffer to hold enough samples "
                    + minBufferSize + " was: " + bufferSize);
            bufferSize = minBufferSize;
        }

        return bufferSize;
    }
    
    // http://stackoverflow.com/questions/10324355/how-to-convert-16-bit-pcm-audio-byte-array-to-double-or-float-array

    public float[] floatMe() {
    
    Log.d(TAG, "floatMe");
    float[] floaters = new float[sound.size()];
    for (int i = 0; i < sound.size(); i++) {
        floaters[i] = sound.get(i);
    }
    sound.clear();
    return floaters;
}

    /**
     * Records audio until stopped the {@link #task} is canceled,
     * {@link #continueRecording} is false, or {@link #clipListener} returns
     * true <br>
     * records audio to a short [readBufferSize] and passes it to
     * {@link #clipListener} <br>
     * uses an audio buffer of size bufferSize * bufferIncreaseFactor
     * 
     * @param recordingBufferSize
     *            minimum audio buffer size
     * @param readBufferSize
     *            reads a buffer of this size
     * @param bufferIncreaseFactor
     *            to increase recording buffer size beyond the minimum needed
     */
    private boolean doRecording(final int sampleRate, int encoding,
            int recordingBufferSize, int readBufferSize, 
            int bufferIncreaseFactor)
    {
        if (recordingBufferSize == AudioRecord.ERROR_BAD_VALUE)
        {
            Log.e(TAG, "Bad encoding value, see logcat");
            return false;
        }
        else if (recordingBufferSize == AudioRecord.ERROR)
        {
            Log.e(TAG, "Error creating buffer size");
            return false;
        }

        // give it extra space to prevent overflow
        int increasedRecordingBufferSize = 
            recordingBufferSize * bufferIncreaseFactor;

        recorder =
                new AudioRecord(AudioSource.MIC, sampleRate,
                        AudioFormat.CHANNEL_IN_MONO, encoding,
                        increasedRecordingBufferSize);

        final short[] readBuffer = new short[readBufferSize];

        continueRecording = true;
        Log.d(TAG, "start recording, " + "recording bufferSize: "
                + increasedRecordingBufferSize 
                + " read buffer size: " + readBufferSize);

        //Note: possible IllegalStateException
        //if audio recording is already recording or otherwise not available
        //AudioRecord.getState() will be AudioRecord.STATE_UNINITIALIZED
        recorder.startRecording();
        
        int i = 0;
        while (continueRecording)
        {
            int bufferResult = recorder.read(readBuffer, 0, readBufferSize);
            //in case external code stopped this while read was happening
            if ((!continueRecording) || ((task != null) && task.isCancelled()))
            {
                break;
            }
            // check for error conditions
            if (bufferResult == AudioRecord.ERROR_INVALID_OPERATION)
            {
                Log.e(TAG, "error reading: ERROR_INVALID_OPERATION");
            }
            else if (bufferResult == AudioRecord.ERROR_BAD_VALUE)
            {
                Log.e(TAG, "error reading: ERROR_BAD_VALUE");
            }
            else
            // no errors, do processing
            {
                //heard = clipListener.heard(readBuffer, sampleRate);
				 for(Short s : readBuffer)
                   sound.add(s);
                 Log.d(TAG, "while loop, sound.size() = " + sound.size());  
            }
            
            if (recordingBufferSize < sound.size())
            {
               continueRecording = false;
            }            
            
        }
        done();
      
        Log.d(TAG, "Recording ended, sound.size() = " + sound.size());
        
        return heard;
    }

    public boolean isRecording()
    {
        Log.d(TAG, "is recording? " + continueRecording); 
        return continueRecording;
    }

    public void stopRecording()
    {
        Log.d(TAG, "stop recording "); 
        continueRecording = false;
    }

    /**
     * need to call this when completely done with recording
     */
    public void done()
    {
        Log.d(TAG, "shut down recorder");
        if (recorder != null)
        {
            recorder.stop();
            recorder.release();
            recorder = null;
        }
    }

}